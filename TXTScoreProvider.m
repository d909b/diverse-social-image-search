classdef TXTScoreProvider
    %TXTScoreProvider Wraps the descriptor cache of the 
    % textual models in the given data set.
    %
    % Author: Patrick Schwab
    
    properties (Constant)
        correspondencesFilename = 'poiNameCorrespondences.txt'; 
    end
    
    properties (Access = private)
        datasetPath;
        cache;
        mapping;
    end
    
    methods (Static)
        function nameCache = parsePOINameCorrespondences(datasetPath)
            filepath = fullfile(datasetPath, TXTScoreProvider.correspondencesFilename);
            
            fd = fopen(filepath, 'r');
            c = onCleanup(@()fclose(fd));
            
            nameCache = java.util.HashMap;
            
            while 1
                name = fscanf(fd, '%[^\t]s', 1);
                
                if isempty(name)
                   break; 
                end
                
                fread(fd, 1, '*char');
                value = fscanf(fd, '%[^\n]s', 1);
                fread(fd, 1, '*char');
                
                nameCache.put(name, value);
            end
        end
        
        function cache = parseTXTDescriptor(datasetPath, descriptorFilepath)
        %PARSETXTDESCRIPTOR Parses the textual descriptor file structure 
        % (probabilistic, tfidf,..) of the devset into a map of key-(key-value)
        % entries. Where the first layer maps the collection to its (term, value)
        % pairs and the second layer maps the term to its calculated feature value.
        %
        % @param descriptorFilepath File to be parsed into the cache
        %   structure.
        %
        % @returns cache The internal cache used for feature vector
        %   queries.
        %
        % @throws TXTScoreProvider:FeatureFileNotReadable If the file at
        %   __filepath__ could not be read.

            nameCache = TXTScoreProvider.parsePOINameCorrespondences(datasetPath);
        
            cache = java.util.HashMap;

            fd = fopen(descriptorFilepath, 'r');

            if fd == -1
                ex = MException('TXTScoreProvider:FeatureFileNotReadable', ...
                                ['The descriptor file could not be opened.' ...
                                '[ path = ' descriptorFilepath ']']);
                throw(ex);
            end
            
            % Register exit handler
            c = onCleanup(@()fclose(fd));

            while 1
                % Collection name ends when the first term starts (")
                name = fscanf(fd, '%[^"]s', 1);
                name = name(1:end-1);

                if isempty(name)
                    break;
                end

                localCache = java.util.LinkedHashMap;

                % Collection lists are 'lf' (ascii value = 10) delimited
                while fread(fd, 1, '*char') == '"'
                    % Special string parsing for unicode tags
                    bytes = [];
                    while 1
                       raw = fread(fd, 1, '*uint8');
                       
                       if raw ~= uint8('"')
                          bytes(end+1) = raw;
                       else 
                          break;
                       end
                    end
                    
                    term = native2unicode(bytes, 'UTF-8');
                    metrics = fscanf(fd, ' %f %f %f');
                    
                    % 3rd one is TFIDF
                    localCache.put(term, metrics(3));
                end
                % Revert position back by one character
                fseek(fd, ftell(fd) - 1, 'bof');
                
                % We have to convert the name to the format in the topics.xml,
                % because of inconsistencies.
                name = nameCache.get(name);
                
                cache.put(name, localCache);
            end
        end 
        
        function cache = getCachedDescriptors(datasetPath, descriptorFilepath)
            cachePath = fullfile(CombinedImageRanker.cacheDirectory, ...
                     [urlencode(descriptorFilepath) '.mat']);
            try
                load(cachePath, 'cache');
            catch err
                cache = TXTScoreProvider.parseTXTDescriptor(datasetPath, descriptorFilepath);
                save(cachePath, 'cache');
            end
        end
        
        function mapping = createTermIndexMapping(datasetPath, descriptorFilepath)
            descriptors = TXTScoreProvider.getCachedDescriptors(datasetPath, descriptorFilepath);
            
            mapping = java.util.HashMap;
            
            names = descriptors.keySet().toArray();
            for i=1:numel(names)
                terms = descriptors.get(names(i)).keySet().toArray();
                localMapping = java.util.HashMap;
                for j=1:numel(terms)
                    localMapping.put(terms(j), j);
                end
                mapping.put(names(i), localMapping);
            end
        end
        
        function mapping = getCachedMapping(datasetPath, descriptorFilepath)
            cachePath = fullfile(CombinedImageRanker.cacheDirectory, ...
                     [urlencode(descriptorFilepath) '_mapping.mat']);
            try
                load(cachePath, 'mapping');
            catch err
                mapping = TXTScoreProvider.createTermIndexMapping(datasetPath, descriptorFilepath);
                save(cachePath, 'mapping');
            end
        end
    end
    
    methods
        function self = TXTScoreProvider(datasetPath, txtFilePath)
            self.datasetPath = datasetPath;
            self.cache = TXTScoreProvider.getCachedDescriptors(datasetPath, txtFilePath);
            self.mapping = TXTScoreProvider.getCachedMapping(datasetPath, txtFilePath);
        end
        
        function score = getFeature(self, query, document)
            tfidfs = self.cache.get(query);
            indices = self.mapping.get(query);
            
            score = zeros(1, tfidfs.size());
            for i=1:numel(document)
                num = tfidfs.get(document{i});
                if ~isempty(num)
                    idx = indices.get(document{i});
                    score(idx) = score(idx) + num;
                end
            end
            
            n = norm(score);
            if abs(n) > 0.0001
                score = score / n;
            end
        end
        
        function score = getReferenceVector(self, query)
            tfidfs = self.cache.get(query);
            score = arrayfun(@(x) x.getValue(), tfidfs.entrySet().toArray())';
            
            n = norm(score);
            if abs(n) > 0.0001
                score = score / n;
            end
        end
    end
end

