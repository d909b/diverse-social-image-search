classdef AlternatingClusterRanker
    %ALTERNATINGCLUSTERRANKER This cluster ranker returns the most
    %relevant images from each cluster.
    
    methods
        function self = AlternatingClusterRanker()
        end
        
        function images = rank(self, clusters)
            allImages = [clusters.images];
            
            images = ImageItem.empty(0,0);
            % Keep going until theres no more images in the clusters
            while numel(images) ~= numel(allImages)
                results = ImageItem.empty(0,0);
                
                % Take the most relevant image from each cluster
                for i=1:size(clusters, 2)
                   relevance = [clusters(i).images.relevance];

                   if numel(relevance) == 0
                      continue
                   end

                   idx = find(relevance == max(relevance));
                   idx = idx(1);

                   best = clusters(i).images(idx);
                   clusters(i).images(idx) = [];
                   results(end + 1) = best;
                end

                % Sort them by relevancy
                relevances = arrayfun(@(img)img.relevance, results);
                [~, sortIdx] = sort(relevances, 'descend');
                
                % Append this rounds results
                images = [images results(sortIdx)];
            end
        end
    end
    
end

