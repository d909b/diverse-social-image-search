classdef Distance
    %DISTANCE Ranks images by their feature-space distance to a reference
    %   image. The distance metric is variable, may be any of the distance
    %   metrics supported by __pdist__.
    %
    % Author: Patrick Schwab
    
    properties(Access = private)
        metric;
        featureSet;
        descriptorProvider;
        postProcessing;
    end
    
    methods
        function self = Distance(metric, descriptorProvider, featureSet, postProcessing)
            self.metric = metric;
            self.featureSet = featureSet;
            self.descriptorProvider = descriptorProvider;
            self.postProcessing = postProcessing;
        end
        
        function images = query(self, topic, numResults)
            [features, references] = Utility.get_feature_vector(topic, ...
                                                                self.featureSet, ...
                                                                self.descriptorProvider, ...
                                                                self.postProcessing);
            
            images = topic.images;
            for i=1:size(images, 2)
                relevance = realmax();
                
                % Score images by their minimum distance to one of the reference vectors.
                for j=1:numel(topic.wikiImages)
                    reference = references(j, :);
                    
                    if strcmpi(self.metric, 'cosine-similarity')
                        denom = (norm(reference)*norm(features(i, :)));
                        if abs(denom) > 0.000001
                            tmp = dot(reference, features(i, :)) ...
                                      / denom;
                        else
                            tmp = 1.; % No information - assume max distance
                        end
                    elseif strcmpi(self.metric, 'histogram-intersection')
                        tmp = sum(min(features(i, :),reference))/min(sum(features(i, :)),sum(reference));
                    elseif strcmpi(self.metric, 'chi2')
                        tmp = vl_alldist2(reference', features(i,:)', 'CHI2');
                    else
                        tmp = pdist2(features(i, :), reference, ...
                                     self.metric);
                    end

                    if ~isnan(tmp) && tmp < relevance
                        relevance = tmp;
                    end
                end
                
                images(i).relevance = relevance;
            end
            
            [~, idx] = sort([images.relevance]);
            images = images(idx);
            
            if exist('numResults', 'var')
                if numResults <= size(images, 2)
                    images = images(1:numResults);
                end
            end
        end
    end
    
end

