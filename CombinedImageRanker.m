classdef CombinedImageRanker < handle
    %COMBINEDIMAGERANKER Combines reranking, clustering and cluster ranking
    % components to maximize diversity of the image search results.
    %
    % Author: Patrick Schwab
    
    properties(Constant)
       cacheDirectory = 'cache'; 
    end
    
    properties(Access = protected)
       descriptorProvider;
       reranker;
       clusterer;
       clusterRanker;
    end
    
    properties (SetAccess = private)
       configuration;
       topics;
    end
    
    methods (Static, Access = private)        
        function topics = getCachedTopicsForDataSet(dataset)
            cachePath = fullfile(CombinedImageRanker.cacheDirectory, ...
                                 [urlencode(dataset) '.mat']);
            try
                load(cachePath, 'topics');
                disp('Loaded image and topic metadata from cache for the given dataset.');
            catch err
                topics = CombinedImageRanker.parseTopicsXML(dataset);
                
                if ~exist(CombinedImageRanker.cacheDirectory, 'dir')
                    if ~mkdir(CombinedImageRanker.cacheDirectory)
                        error('Could not create cache folder.');
                    end
                end
                
                save(cachePath, 'topics');
                disp('Cached image and topic metadata for the given dataset.');
            end
        end
        
        function topics = parseTopicsXML(dataset)
            % Find the topics file for this data set
            files = dir(fullfile(dataset, ...
                        '*_topics.xml'));
            xmlPath = fullfile(dataset, files(1).name);
            
            try
                doc = xmlread(xmlPath);
            catch err
            	ex = MException('CombinedImageRanker:TopicFileNotReadable', ...
                 ['The topic XML file could not be read. [ path = ' xmlPath ...
                 ' ]']);
                throw(ex); 
            end
            
            topicNodes = doc.getElementsByTagName('topic');
            
            for i=0:topicNodes.getLength-1
                t = topicNodes.item(i);
                
                title = char(Utility.XMLHelper.dataForTagName(t, 'title'));
                
                %disp(['Loading topic ' title ' - topic #' num2str(i+1) ...
                %      ' / ' num2str(topicNodes.getLength)]);
                  
                topics(i+1) = Topic(dataset, title); 
            end
        end
    end
    
    methods (Access = private)
        function topics = filter(self, topics)
            if self.configuration.filterFaces
                for i=1:numel(topics)
                    imgs = topics(i).images;
                    topics(i).setImages([imgs([imgs.numPeople] == 0) ... % No faces
                                         imgs([imgs.numPeople] > 4)]); % Big groups are most likely OK (as per MediaEval rules)
                end
            end
        end 
    end
    
    methods
        function self = CombinedImageRanker()
            self.topics = [];
            self.clusterRanker = AlternatingClusterRanker();
            
            % Setup vlfeat if it hasnt already been setup.
            try
                vlversion verbose;
            catch err
                run('External/vlfeat-0.9.18/toolbox/vl_setup');
            end
        end
        
        function images = query(self, topic, numResults)
            if isempty(topic.images)
                ex = MException('CombinedImageRanker:NotInitialized', ...
                 'The ranker must be initialized with a dataset before use.');
                throw(ex);
            end
            
            % Rerank the images to increase precision first.
            images = self.reranker.query(topic);
            
            if ~isempty(self.configuration.clusterFeatures)
                features = Utility.get_feature_vector(topic, ...
                                                      self.configuration.clusterFeatures, ...
                                                      self.descriptorProvider, ...
                                                      self.configuration.clusteringPostProcessing);
                % Determine clusters.
                clusters = self.clusterer.cluster(images, ...
                                                  features, ...
                                                  self.configuration.clusteringMetric);

                % Pick images from clusters taking their relevancy and
                % cluster mapping into account.
                images = self.clusterRanker.rank(clusters);
            end
            
            % Cut off the result list if we only need a limited amount of
            % results.
            if exist('numResults', 'var')
                if numResults <= size(images, 2)
                    images = images(1:numResults);
                end
            end
            
            % div_eval.jar requires the relevancies to be descending
            % numbers, thus we assign them a relevancy rating based on 
            % their sort index. 1 (highest) to 1/n (lowest)
            rankings = (numel(images):-1:1) / numel(images);
            for i=1:numel(images)
                images(i).relevance = rankings(i);
            end
        end
        
        function [] = loadDataSet(self, path, configuration)
            self.configuration = configuration; % This needs to be set before call to filter -
                                                % however a full setup is
                                                % only performed on __setConfiguration__.
                                                
            self.topics = self.filter(CombinedImageRanker.getCachedTopicsForDataSet(path));
            self.descriptorProvider = DescriptorProvider(path, self.topics);
            
            self.setConfiguration(configuration);
        end
        
        function [] = setConfiguration(self, configuration)
           self.configuration = configuration;
           
           % Dynamically create class instances from their name using
           % package syntax.
           self.clusterer = Clusterer.(configuration.clustering)();
           self.reranker = Reranker.(configuration.reranking)( ...
               configuration.rankingMetric, self.descriptorProvider, ...
               configuration.rankingFeatures, configuration.rankingPostProcessing);
        end
    end
    
end

