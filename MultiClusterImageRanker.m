classdef MultiClusterImageRanker < CombinedImageRanker
    %MULTICLUSTERIMAGERANKER 
    %
    % Author: Patrick Schwab
    
    properties
    end
    
    methods
        function self = MultiClusterImageRanker()
            self@CombinedImageRanker();
            self.clusterRanker = IntersectionClusterRanker();
        end
        
        function images = query(self, topic, numResults)
            if isempty(topic.images)
                ex = MException('MultiClusterImageRanker:NotInitialized', ...
                 'The ranker must be initialized with a dataset before use.');
                throw(ex);
            end
            
            % Rerank the images to increase precision first.
            images = self.reranker.query(topic);
            
            clusters = {};
            if ~isempty(self.configuration.clusterFeatures)
                for i=1:numel(self.configuration.clusterFeatures)
                    features = Utility.get_feature_vector(topic, ...
                                                          self.configuration.clusterFeatures(i), ...
                                                          self.descriptorProvider, ...
                                                          self.configuration.clusteringPostProcessing);
                    % Determine clusters.
                    clusters{i} = self.clusterer.cluster(images, ...
                                                         features, ...
                                                         self.configuration.clusteringMetric);
                end
            end
            
            
            % Pick images from clusters taking their relevancy and
            % cluster mapping into account.
            images = self.clusterRanker.rank(clusters);
            
            % Cut off the result list if we only need a limited amount of
            % results.
            if exist('numResults', 'var')
                if numResults <= size(images, 2)
                    images = images(1:numResults);
                end
            end
            
            % div_eval.jar requires the relevancies to be descending
            % numbers, thus we assign them a relevancy rating based on 
            % their sort index. 1 (highest) to 1/n (lowest)
            rankings = (numel(images):-1:1) / numel(images);
            for i=1:numel(images)
                images(i).relevance = rankings(i);
            end
        end
    end
    
end

