-- INSTALLATION --

To use the workflow application a recent installation of Matlab is required. 
Additionally, a MediaEval challenge data set, as defined by the data set overview document of MediaEval 2014, is required.

-- QUICK START TUTORIAL --

To use the workflow application you first need to instantiate an EvaluationOutput object, e.g.:

ev = EvaluationOutput();

The second required step is to define a configuration to be used for the following evaluation. Here, we use the utility method defaultDescriptorConfiguration to conveniently create a pre-defined, general-purpose configuration.

config = DescriptorConfiguration.defaultDescriptorConfiguration();

Next, you must point the EvaluationOutput instance to the MediaEval data set you wish to evaluate. This call performs preloading and parsing of the images’ metadata. Thus, it might take a few minutes to complete.

ev.loadDataSet(‘path/to/your/dataset’, config);

Lastly, you have to call the evaluate() method on the EvaluationOutput object to start a new run with your data set and configuration. Note that the output directory must already exist.

results = ev.evaluate('path/to/output/file.txt', 'myrunname');

The resulting output file will be written after the workflow has finished processing your request. The results variable will contain an array of EvaluationResult objects corresponding to the EvaluationResults at each cutoff point X.
