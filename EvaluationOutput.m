classdef EvaluationOutput < handle
    %EVALUATIONOUTPUT Produces evaluation data using a given workflow
    % configuration on a MediaEval dataset. The evaluation data contains
    % averaged P/CR/F1@X statistics over all possible location queries.
    %
    % Author: Patrick Schwab
    
    properties (Constant, Access = private)
        maxResults = 50;
        runfileFolder = 'runfiles';
    end
    
    properties (Access = private)
        imageRanker;
        dataSetPath;
    end
    
    methods (Access = private)
        function [] = writeRunFile(self, outputFilePath, runName)
            fd = fopen(outputFilePath, 'w');
            
            if fd == -1
               error(['Invalid filepath: ' outputFilePath]); 
            end
            
            c = onCleanup(@()fclose(fd));
            
            %matlabpool ('open', 2);
            %m = onCleanup(@()matlabpool('close'));
            
            for i=1:size(self.imageRanker.topics, 2)
                topic = self.imageRanker.topics(i);
                imgs = self.imageRanker.query(topic, ...
                                              EvaluationOutput.maxResults);
                
                for j=1:size(imgs, 2)
                    fprintf(fd, '%d 0 %d %d %.4f %s\n', str2double(topic.number), ...
                        str2double(imgs(j).imageID), j-1, ...
                        imgs(j).relevance, runName);
                end
            end
        end
        
        function success = calculateMetrics(self, runFilePath, evaluationFilePath)
            % Find the topics file for this data set
            files = dir(fullfile(self.dataSetPath, ...
                        '*_topics.xml'));
            topicsXML = fullfile(self.dataSetPath, files(1).name);
            
            [path, name, ext] = fileparts(evaluationFilePath);
            
            status = system(['java -jar div_eval.jar -r ' runFilePath ...
                ' -rgt ' self.dataSetPath '/gt/rGT ' ...
                '-dgt ' self.dataSetPath '/gt/dGT ' ...
                '-t ' topicsXML ' -o ' path ...
                ' -f ' [name ext] ...
                ]);
            
            success = status == 0;
        end
        
        function results = parseEvaluationFile(self, evaluationFilePath)
            fd = fopen(evaluationFilePath, 'r');
            
            if fd == -1
               error(['Invalid filepath: ' ...
                       evaluationFilePath]); 
            end
            
            c = onCleanup(@()fclose(fd));
            
            stopLine = '"--"';
            while 1
                line = fgetl(fd);

                if strfind(line, stopLine) == 1
                    break
                end
            end
            
            fscanf(fd, ',', 1);
            
            results(6) = EvaluationResult;
            cutoffPoints = [5, 10, 20, 30, 40, 50];
            i = 0;
            metric = fscanf(fd, ',%f', 1);
            while ~isempty(metric)
                
                switch floor(i/6)
                    case 0
                        results(mod(i, 6)+1).precision = metric;
                        results(mod(i, 6)+1).cutoffPoint = cutoffPoints(i+1);
                    case 1
                        results(mod(i, 6)+1).clusterRecall = metric;
                    case 2
                        results(mod(i, 6)+1).f1 = metric;
                end
                i=i+1;
                
                metric = fscanf(fd, ',%f', 1);
            end
        end
    end
    
    methods
        function self = EvaluationOutput()
            self.imageRanker = CombinedImageRanker();
        end
        
        function results = evaluate(self, outputFilePath, runName)
            if ~exist(EvaluationOutput.runfileFolder, 'dir')
                if ~mkdir(EvaluationOutput.runfileFolder)
                    error('Could not create runfile folder.');
                end
            end
            
            runFilePath = fullfile(EvaluationOutput.runfileFolder, ...
                [runName '.txt']);
            
            self.writeRunFile(runFilePath, runName);
            if self.calculateMetrics(runFilePath, outputFilePath)
                results = self.parseEvaluationFile(outputFilePath);
                [results.configuration] = deal(self.imageRanker.configuration);
            else
                error('Failed to invoke div_eval.jar to calculate the retrieval metrics.');
            end
        end
        
        function [] = loadDataSet(self, path, configuration)
            if nargin < 3
               configuration = DescriptorConfiguration.defaultDescriptorConfiguration(); 
            end
            
            self.dataSetPath = path;
            self.imageRanker.loadDataSet(path, configuration);
        end
        
        function [] = setConfiguration(self, configuration)
            self.imageRanker.setConfiguration(configuration);
        end
    end
    
end

