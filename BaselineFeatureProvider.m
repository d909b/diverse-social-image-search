classdef BaselineFeatureProvider
    %BASELINEFEATUREPROVIDER Provides the baseline (=initial flickr rank)
    % ranking as a feature.
    
    properties (Constant = true)
       maxImagesPerLocation = 150; % According to the dataset description
    end
    
    properties (SetAccess = private)
        type;
    end
    
    methods
        function self = BaselineFeatureProvider()
           self.type = FeatureType.Baseline; 
        end
        
        function feature = getFeature(self, query, image)
            if image.isWikiImage
                feature = 1.0;
            else
                feature = (BaselineFeatureProvider.maxImagesPerLocation - image.rank) ...
                    / BaselineFeatureProvider.maxImagesPerLocation;
            end
        end
    end
    
end

