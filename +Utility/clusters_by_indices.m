function clusters = clusters_by_indices( images, data2cluster )
%CLUSTERS_BY_INDICES Assigns the images in __images__ to clusters by their 
% corresponding __data2cluster__ entries.
%
% @param images An array of ImageItems @see ImageItem.m (1xn)
%
% @param data2cluster An array of indices connecting the __images__ to 
%   their clusters (1xn, with n .. number of images).
% 
% @returns clusters The clusters with the assigned images.
%
% Author: Patrick Schwab

if size(data2cluster, 2) < size(data2cluster,1 )
   data2cluster = data2cluster'; 
end

i=1;
% Fill clusters according to the matching made by the clustering algorithm
for n=unique(data2cluster)
    clusters(i) = Cluster(images(data2cluster == n));
    i=i+1;
end

end

