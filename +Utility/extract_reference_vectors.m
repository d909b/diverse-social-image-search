function features = extract_reference_vectors( topic, featureSet, ...
       descriptorProvider )
%EXTRACT_REFERENCE_VECTORS 
%
% Author: Patrick Schwab

images = topic.wikiImages;

for i=1:numel(images)
    for j=1:numel(featureSet)
        features{i,j} = descriptorProvider.getFeature( ...
            topic.name, images(i), featureSet(j));
    end
end

features = cell2mat(features);
% 
% for k=1:size(features, 2)
%     feature = features(:, k);
%     l = min(feature);
%     u = max(feature);
%     
%     if u - l > 0.001
%         % Normalize each feature to the range [0, 1] individually
%         features(:, k) = (feature - l) / (u - l);
%     end
% end

end

