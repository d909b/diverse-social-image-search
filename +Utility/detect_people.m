function count = detect_people(imagepath)
%DETECT_PEOPLE Returns the number of people detected in an image.
%
% Author: Patrick Schwab
% CREDITS: Based on ped_demo.m using ped-demo-fast-8x8x1.1
% CREDITS2: Based on FPDW DemoInria.m

persistent hasLoaded;

base = fullfile(pwd, 'External/FaceDetectionMATLAB-source');
if isempty(hasLoaded)
    addpath(base);
    
    hasLoaded = true;
end

try
    det = viola(imagepath, ...
        fullfile(base, 'haarcascade_frontalface_default_2.xml'));
    count = det.nFaces;
catch err
   warning(['Failed to detect people in image at: ' imagepath ...
       ', because: ' err.message]);
   count = 0; 
end

% addpath('/Users/patrick/Dropbox/UNI/Sem2/MMPR/Z1/project2014/External/libsvm-mat-2.84-1-fast.v3', ...
%   '/Users/patrick/Dropbox/UNI/Sem2/MMPR/Z1/project2014/External/ped-demo-fast-8x8x1.1');
%
% try
%     % Calling the function using evalc and dropping the output, because it is verbose.
%     [~, ~, scales] = evalc('detect_people(imagepath);');
% 
%     count = numel(scales);
% catch err
%     disp(['Failed to detect people in image at: ' imagepath]);
%     count = 0; 
% end

end

