function [] = plot_results( field, yname, results, legends )

    colors = {'k', '-go', '-bs', '-mx'};

    figure('Color', [1 1 1]);
    set(gcf, 'defaulttextinterpreter', 'latex');

    hold on;

    extract_cutoff = @(r) r.cutoffPoint;
    extract = @(r) getfield(r, field);
    for i=1:size(results,2)
        plot(arrayfun(extract_cutoff, results{i}), ...
             arrayfun(extract, results{i}), ...
             colors{mod(i, numel(colors))}, ...
             'LineWidth', 1.5);
    end

    hold off;

    %ylim([0 1]);
    ylabel(yname);
    xlabel('@X');
    
    if nargin > 3
        legend(legends);
    end
end

