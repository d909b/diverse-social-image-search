function [ features, references ] = get_feature_vector(topic, featureSet, ...
       descriptorProvider, config )
%GET_FEATURE_VECTOR
%
% Author: Patrick Schwab

if strcmpi(config, 'None')
    features = Utility.extract_feature_vector(topic, featureSet, descriptorProvider);
    references = Utility.extract_reference_vectors(topic, featureSet, descriptorProvider);
elseif strcmpi(config, 'Normalize')
    [features, references] = Utility.get_normalized_feature_vectors(topic, featureSet, descriptorProvider);
elseif strcmpi(config, 'Decorrelate')
    [features, references] = Utility.get_decorrelated_feature_vectors(topic, featureSet, descriptorProvider);
end

end

