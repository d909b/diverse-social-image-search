function results = evaluate_all_descriptors(datasetPath, outputDir)
%EVALUATE_ALL_DESCRIPTORS Evaluates all possible descriptors individually.
%
% @param datasetPath The dataset on which the results are computed.
%
% @param outputDir The directory to which the evaluation result files
%   should be written.
%
% @returns results The evaluation results for each configuration. 
%   @see EvaluationResult
%
% Author: Patrick Schwab

rankingMetrics = {'euclidean', 'cityblock', 'chi2'};
clusterMethods = {'AHC'};
featureTypes = enumeration('FeatureType');

ev = EvaluationOutput();
ev.loadDataSet(datasetPath, ...
               DescriptorConfiguration.defaultDescriptorConfiguration());

runname = @(i,j,k,rc) [char(featureTypes(i)) '_' rankingMetrics{j} '_' clusterMethods{k} '_' rc];           
outputname = @(i,j,k,rc) fullfile(outputDir, [runname(i,j,k,rc) '.txt']);

results = {};

for i=1:numel(featureTypes)
    for j=1:numel(rankingMetrics)
        config = DescriptorConfiguration.defaultDescriptorConfiguration();
        
%         if FeatureType.isTextual(featureTypes(i)) && j == 1 || ...
%            FeatureType.isVisual(featureTypes(i)) && j == 2
%             continue
%         end
        
        % Ranking-only
        config.rankingFeatures = featureTypes(i);
        config.clusterFeatures = [];
        config.rankingMetric = rankingMetrics{j};
        
        ev.setConfiguration(config);
        results{end + 1} = ev.evaluate(outputname(i,j,1,'rel'), runname(i,j,1,'rel'));
            
        for k=1:numel(clusterMethods)
            for l=1:numel(rankingMetrics)
%                 if i == 1
%                    break;
%                 end

                config = DescriptorConfiguration.defaultDescriptorConfiguration();

                % Clustering-only
                config.rankingFeatures = FeatureType.Baseline;
                config.rankingMetric = rankingMetrics{j};
                
                config.clusterFeatures = featureTypes(i);
                config.clusteringMetric = rankingMetrics{l};
                config.clustering = clusterMethods{k};

                ev.setConfiguration(config);
                results{end + 1} = ev.evaluate(outputname(i,j,k,'cluster'), runname(i,j,k,'cluster'));
            end
        end
    end
end

end

