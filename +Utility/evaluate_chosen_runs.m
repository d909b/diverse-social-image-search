function [text_results, vis_results, comb_results, ev] = evaluate_chosen_runs(datasetPath, outputDir, ev)
%EVALUATE_CHOSEN_RUNS Evaluates specifically chosen run configurations and
% returns the corresponding results.
%
% @param datasetPath The dataset on which the results are computed.
%
% @param outputDir The directory to which the evaluation result files
%   should be written
%
% @return text_results The evaluation results for the textual runs (type: 1xn
%                @see EvaluationResults)
%
% @return vis_results The evaluation results for the visual runs (type: 1xn
%                @see EvaluationResults)
%
% @return comb_results The evaluation results for the combined runs (type: 1xn
%                @see EvaluationResults)
%
% Author: Patrick Schwab

if nargin < 3
    ev = EvaluationOutput();
    ev.loadDataSet(datasetPath, ...
                   DescriptorConfiguration.defaultDescriptorConfiguration());
end
                 
outputname = @(config) fullfile(outputDir, [runname(config) '.txt']);

textConfigs = buildConfigSet(...
    {FeatureType.TFIDF}, ...
    {FeatureType.TFIDF});
 
visConfigs = buildConfigSet(...
    { ...
     FeatureType.GLRLM, ...
     FeatureType.GLRLM3x3, ...
     FeatureType.SIFT
    }, ...
    { ...
     FeatureType.GLRLM3x3, ...
     FeatureType.HOG, ...
     FeatureType.LBP, ...
     FeatureType.LBP3x3, ...
     [FeatureType.LBP, FeatureType.LBP3x3], ...
     FeatureType.SIFT, ...
     [FeatureType.SIFT, FeatureType.LBP3x3]
    }, ... 
    false);

combConfigs = buildConfigSet(...
    { ...
     FeatureType.TFIDF, ...
     FeatureType.GLRLM3x3, ...
     FeatureType.SIFT, ...
     [FeatureType.TFIDF, FeatureType.GLRLM3x3], ...
     [FeatureType.TFIDF, FeatureType.SIFT, FeatureType.GLRLM3x3], ...
     [FeatureType.TFIDF, FeatureType.SIFT]
    }, ...
    { ...
     FeatureType.TFIDF, ...
     [FeatureType.TFIDF, FeatureType.HOG], ...
     FeatureType.HOG, ...
     FeatureType.LBP3x3, ...
     FeatureType.SIFT, ...
     [FeatureType.SIFT, FeatureType.LBP], ...
     [FeatureType.SIFT, FeatureType.LBP3x3]
    });

text_results = evaluateConfigSet(textConfigs);
vis_results = evaluateConfigSet(visConfigs);
comb_results = evaluateConfigSet(combConfigs);

    function name = runname(config)
        cRanking = arrayfun(@char, config.rankingFeatures, 'UniformOutput', false);
        cClustering = arrayfun(@char, config.clusterFeatures, 'UniformOutput', false);
        
        name = ['p-' rankingMetricToCode(config.rankingMetric) '_' ...
                cRanking{:} ...
                '_cr-' rankingMetricToCode(config.clusteringMetric) '_' ...
                cClustering{:} ...
                '_' clusteringAlgorithmToCode(config.clustering)]; 
    end

    function str = clusteringAlgorithmToCode(clustering)
        if(strcmpi(clustering, 'KMeans'))
            str = 'KM';
        elseif(strcmpi(clustering, 'AHC'))
            str = 'AHC';
        elseif(strcmpi(clustering, 'MeanShift'))
            str = 'MS';
        else
            str = 'ERR';
        end
    end

    function str = rankingMetricToCode(metric)
        if(strcmpi(metric, 'euclidean')) || strcmpi(metric, 'L2')
            str = 'L2';
        elseif strcmpi(metric, 'cosine-similarity')
            str = 'CS';
        elseif strcmpi(metric, 'cityblock') || strcmpi(metric, 'L1')
            str = 'L1';
        elseif strcmpi(metric, 'chi2') 
            str = 'X2';
        elseif strcmpi(metric, 'correlation') 
            str = 'PR';
        else
            str = 'ERR';
        end
    end

    function configs = buildConfigSet(rankingFeatures, clusterFeatures, includeCS)
        if nargin < 3
           includeCS = true; 
        end
        
        if includeCS
            rankingMetrics = {'euclidean', 'cosine-similarity', 'cityblock'};
        else
            rankingMetrics = {'euclidean', 'cityblock'};
        end
        
        % Override for this run
        rankingMetrics = {'chi2', 'cosine-similarity'};
        
        clusterMethods = {'AHC'};

        configs = {};
        for i=1:numel(rankingFeatures)
            for j=1:numel(clusterFeatures)
                for k=1:1%numel(rankingMetrics)
                    for l=1:numel(clusterMethods)
                        for m=1:numel(rankingMetrics)
                            config = DescriptorConfiguration.defaultDescriptorConfiguration();

                            config.rankingFeatures = rankingFeatures{i};
                            config.rankingMetric = rankingMetrics{k};
                            
                            config.clusterFeatures = clusterFeatures{j};
                            config.clusteringMetric = rankingMetrics{m};
                            config.clustering = clusterMethods{l};

                            configs{end+1} = config;
                        end
                    end
                end
            end
        end
    end

    function results = evaluateConfigSet(configs)
        results = cell(1, numel(configs));
        for i=1:numel(configs)
            ev.setConfiguration(configs{i});
            results{i} = ev.evaluate(outputname(configs{i}), runname(configs{i}));
        end
    end
end

