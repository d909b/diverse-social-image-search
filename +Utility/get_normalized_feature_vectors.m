function [ features, references ] = get_normalized_feature_vectors( topic, featureSet, ...
       descriptorProvider )
%GET_NORMALIZED_FEATURE_VECTORS 
%
% Author: Patrick Schwab

features = Utility.extract_feature_vector(topic, featureSet, descriptorProvider);
references = Utility.extract_reference_vectors(topic, featureSet, descriptorProvider);

vectors = [features; references;];

vectors = zscore(vectors);

features = vectors(1:size(features,1), :);
references = vectors(size(features,1)+1:end, :);

end

