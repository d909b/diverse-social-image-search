function [ ] = perform_evaluation( datasetPath )
%PERFORM_EVALUATION
%
% Author: Patrick Schwab

outDir = 'img';

results = Utility.evaluate_all_descriptors(datasetPath, 'evaluation');

save('cache/results.mat', 'results');

[res_txt, res_vis, res_cmb] = Utility.evaluate_chosen_runs(datasetPath, 's_evaluation');

save('cache/res_txt.mat', 'res_txt');
save('cache/res_vis.mat', 'res_vis');
save('cache/res_cmb.mat', 'res_cmb');

Utility.visualize_evaluation(results, outDir, res_txt, res_vis, res_cmb);

end

