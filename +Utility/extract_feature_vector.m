function features = extract_feature_vector( topic, featureSet, ...
       descriptorProvider)
%EXTRACT_FEATURE_VECTOR Extracts a feature vector matrix for the given
%topic. 
%
% @param topic The topic for whose images a feature vector matrix should be
%   computed. Contains n images.
%
% @param featureSet An array of featureTypes @see FeatureType.m defining
%   which features should be included in the feature matrix.
%
% @returns features A feature matrix where each row j corresponds to a
%   feature vector for image at index i. (nxm, where n .. number of images,
%   and m .. number of features)
%
% Author: Patrick Schwab

images = topic.images;

for i=1:numel(images)  
    img = images(i);
    
    for j=1:numel(featureSet)
        features{i,j} = descriptorProvider.getFeature( ...
            topic.name, img, featureSet(j));
    end
end

features = cell2mat(features);
% 
% for k=1:size(features, 2)
%     feature = features(:, k);
%     l = min(feature);
%     u = max(feature);
%     
%     if u - l > 0.001
%         % Normalize each feature to the range [0, 1] individually
%         features(:, k) = (feature - l) / (u - l);
%     end
% end

end

