classdef XMLHelper
    %XMLHELPER Utility class for dealing with XML files.
    %
    % Author: Patrick Schwab
    
    methods (Static)
        function data = dataForTagName(xmlNode, tagName)
            data = xmlNode.getElementsByTagName(tagName).item(0).getFirstChild.getData;
        end
    end
end

