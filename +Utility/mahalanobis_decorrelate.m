function [ z ] = mahalanobis_decorrelate( x )
%MAHALANOBIS_DECORRELATE Decorrelates the input observations by using
% the Mahalanobis transform.
%
% Author: Patrick Schwab
% CREDITS: Adapted from http://www.davidsalomon.name/DC2advertis/DeCorr.pdf

s=cov(x); % empirical covariance

[eigvec,eigval]=eig(s); % spectral decomposition
eigval=diag(eigval);
tmp=sort([eigval,eigvec],1);

% sort by eigenvalues (col. 1) ascending
tmp=flipud(tmp); % Flip to get descending sort
eigval=tmp(:,1);
[r,c]=size(tmp);
eigvec=tmp(:,2:c)';
eigval1=1./sqrt(eigval); % computes lambda to the power -1/2
eigval1=diag(eigval1); % makes diagonal matrix from lambda2
s2=eigvec*eigval1*eigvec'; % computes s to the power -1/2

x=x - repmat(mean(x, 1), size(x, 1), 1); % corrects for mean
z=x*real(s2); % Mahalanobis transformation

end

