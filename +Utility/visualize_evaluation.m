function [] = visualize_evaluation(results, outDir, res_txt, res_vis, res_cmb)
%VISUALIZE_EVALUATION Produces visualization for the evaluation results.
%
% @param results The result data as produced by Utility.evaluate_all_descriptors
%                                (type EvaluationResult, cell array 1xn, 
%                                 with n ... number of results)
%
% @param res_txt The textual result data as produced by
%                                Utility.evaluate_chosen_runs
%                                (type EvaluationResult, cell array 1xn, 
%                                 with n ... number of results), optional
%
% @param res_vis The visual result data as produced by
%                                Utility.evaluate_chosen_runs
%                                (type EvaluationResult, cell array 1xn, 
%                                 with n ... number of results), optional
%
% @param res_cmb The combined result data as produced by
%                                Utility.evaluate_chosen_runs
%                                (type EvaluationResult, cell array 1xn, 
%                                 with n ... number of results), optional
%
% @param outDir The figure files' output directory
%
% Author: Patrick Schwab

cutoffPoints = [5 10 20 50];
precisionBaseline = containers.Map;
recallBaseline = containers.Map;
colorPalette = {[49,32,38] / 255, [156,159,228] / 255, [255,158,181] / 255};
redGreenPalette = {[196,77,88] / 255, [199,244,100] / 255};
bluePalette = {[65,200,255] / 255, [65,200,255] / 255};

% Produce a graph for each of the chosen cutoff points -- PRECISION
for j=1:numel(cutoffPoints)
    resultSet = containers.Map;
    
    % Collect the best results at the specified cutoffPoint for each
    % feature.
    for k=1:numel(results)
       r = results{k};

       tmp = r(arrayfun(@(x) x.cutoffPoint == cutoffPoints(j), r));

       currentPrecision = 0;
       if resultSet.isKey(char(tmp.configuration.rankingFeatures(1)))
           currentPrecision = resultSet(char(tmp.configuration.rankingFeatures(1))).precision;
       end
       
       if tmp.precision > currentPrecision && numel(tmp.configuration.clusterFeatures) == 0
          resultSet(char(tmp.configuration.rankingFeatures(1))) = tmp;
       end
    end
    
    resultSet = resultSet.values();
    resultSet = [resultSet{:}];

    figure('Color', [1 1 1]);
    hold on;

    plotname = ['Precision @' num2str(cutoffPoints(j))];
    title(plotname, 'fontWeight', 'bold');

    precisions = [resultSet.precision];
    
    % Sort by precision, descending
    [~, idx] = sort(precisions, 2, 'descend');
    resultSet = resultSet(idx);
    
    configs = [resultSet.configuration];
    features = [configs.rankingFeatures];
    try
        base = resultSet(features == FeatureType.Baseline).precision;
        recallBaseline(num2str(cutoffPoints(j))) = resultSet(features == FeatureType.Baseline).clusterRecall;
    catch err
        base = 0;
        recallBaseline(num2str(cutoffPoints(j))) = 0;
    end
    
    precisionBaseline(num2str(cutoffPoints(j))) = base;
        
    [line, barType1, barType2] = createBarPlotWithClasses( ...
                             [resultSet.precision], features, {colorPalette{1:3}}, base);
    
    legend([line barType1 barType2], ...
           'Baseline', 'Textual feature', 'Visual feature');
    
    ylim([0 1]);
    ylabel('Precision');

    metrics = {configs.rankingMetric};
    names = arrayfun(@char, features, 'UniformOutput', false);
    
    % Add the used metric to the feature type
    for o=1:numel(names)
       names{o} = [names{o} '\_' rankingMetricToCode(metrics{o})];
    end

    set(gca, 'XTick', 1:numel(resultSet), 'XTickLabel', names);
    Lib.rotateXLabels(gca(), -45);
    
    % Bring baseline to top
    uistack(line, 'top');

    hold off;
    
    exportFigureAsPDF(['p' num2str(cutoffPoints(j))]);
    
    if nargin > 2 % RES_TXT
        [p, cr, f1] = plotResultSet(res_txt, cutoffPoints(j), '_txt');
    end
    
    if nargin > 3 % RES_VIS
        [p, cr, f1] = plotResultSet(res_vis, cutoffPoints(j), '_vis');
    end
    
    if nargin > 4 % RES_CMB
        [p, cr, f1] = plotResultSet(res_cmb, cutoffPoints(j), '_cmb');
    end
end

% Produce a graph for each of the chosen cutoff points -- RECALL
for j=1:numel(cutoffPoints)
    resultSet = containers.Map;
    
    % Collect the best results at the specified cutoffPoint for each
    % feature.
    for k=1:numel(results)
       r = results{k};

       tmp = r(arrayfun(@(x) x.cutoffPoint == cutoffPoints(j), r));

       if numel(tmp.configuration.clusterFeatures) ~= 1
           continue;
       end
       
       currentRecall = 0;
       if resultSet.isKey(char(tmp.configuration.clusterFeatures(1)))
           currentRecall = resultSet(char(tmp.configuration.clusterFeatures(1))).clusterRecall;
       end
       
       if tmp.clusterRecall > currentRecall
          resultSet(char(tmp.configuration.clusterFeatures(1))) = tmp;
       end
    end
    
    resultSet = resultSet.values();
    resultSet = [resultSet{:}];
    
    figure('Color', [1 1 1]);
    hold on;

    plotname = ['Cluster Recall @' num2str(cutoffPoints(j))];
    title(plotname, 'fontWeight', 'bold');

    recalls = [resultSet.clusterRecall];
    
    % Sort by recall, descending
    [~, idx] = sort(recalls, 2, 'descend');
    resultSet = resultSet(idx);
    
    configs = [resultSet.configuration];
    features = [configs.clusterFeatures];
    
    [line, barType1, barType2] = createBarPlotWithClasses( ...
                             [resultSet.clusterRecall], features, {colorPalette{1:3}}, ...
                             recallBaseline(num2str(cutoffPoints(j))));
    
    if cutoffPoints(j) ~= 50 % Skip legend at 50 because there isnt sufficient
                             % space left in the figure for it.
        legend([line barType1 barType2], ...
               'Baseline', 'Textual feature', 'Visual feature');
    end
    
    ylim([0 1]);
    ylabel('Cluster Recall');

    clustering = {configs.clustering};
    names = arrayfun(@char, features, 'UniformOutput', false);
    
    % Add the used metric to the feature type
    for o=1:numel(names)
       names{o} = [names{o} '\_' ...
                   clusteringAlgorithmToCode(clustering{o})];
    end

    set(gca, 'XTick', 1:numel(resultSet), 'XTickLabel', names);
    Lib.rotateXLabels(gca(), -45);
    
    % Bring baseline to top
    uistack(line, 'top');

    hold off;
    
    exportFigureAsPDF(['cr' num2str(cutoffPoints(j))]);
    
    figure('Color', [1 1 1]);
    hold on;

    plotname = ['Precision Impact @' num2str(cutoffPoints(j))];
    title(plotname, 'fontWeight', 'bold');
    
    createBarPlotWithClasses( ...
             ([resultSet.precision] - precisionBaseline(num2str(cutoffPoints(j))))*100, ...
             features, ...
             {colorPalette{1} redGreenPalette{1:2}});
    
    ylim([-0.15 0.15]*100);
    ylabel('Precision, Difference to Baseline [in %]');

    set(gca, 'XTick', 1:numel(resultSet), 'XTickLabel', names);
    Lib.rotateXLabels(gca(), -45);

    hold off;
    
    exportFigureAsPDF(['pcr' num2str(cutoffPoints(j))]);
end
    
    function str = rankingMetricToCode(metric)
        if(strcmpi(metric, 'cityblock'))
            str = 'L1';
        elseif(strcmpi(metric, 'euclidean'))
            str = 'L2';
        elseif(strcmpi(metric, 'cosine-similarity'))
            str = 'CS';
        elseif(strcmpi(metric, 'histogram-intersection'))
            str = 'HI';
        elseif(strcmpi(metric, 'chi2'))
            str = 'X2';
        else
            str = 'ERR';
        end
    end

    function str = clusteringAlgorithmToCode(clustering)
        if(strcmpi(clustering, 'KMeans'))
            str = 'KM';
        elseif(strcmpi(clustering, 'AHC'))
            str = 'AHC';
        elseif(strcmpi(clustering, 'MeanShift'))
            str = 'MS';
        else
            str = 'ERR';
        end
    end

    function [] = exportFigureAsPDF(filename)
        % CREDITS: Adapted from 
        % http://stackoverflow.com/questions/5150802/how-to-save-a-plot-into-a-pdf-file-without-a-large-margin-around
        set(gcf, 'PaperPosition', [0 -0.25 8 6]); 
        set(gcf, 'PaperSize', [8 6]); 

        % Export figure to pdf
        saveas(gcf, fullfile(outDir, filename), 'pdf');

        close(gcf);
    end

    function [p, b1, b2] = createBarPlotWithClasses(data, featureSet, colors, ...
                                                    baseline)
        if nargin > 3
            % Plot the baseline
            p = plot([0 numel(data) + 0.5], ...
                      [baseline baseline], ...
                     'Color', colors{1}, 'LineStyle', '--');
                 
            comp = @(idx) FeatureType.isTextual(featureSet(idx));
        else
            p = [];
            comp = @(idx) data(idx) < 0;
        end
        
        b1 = [];
        b2 = [];

        firstText = true;
        firstVis = true;
        for l=1:numel(data)
            if comp(l)
                color = colors{2};
            else
                color = colors{3};
            end

            b = bar(l, data(l), 'FaceColor', color);

            if comp(l)
                if firstText
                    b1 = b;
                    firstText = false;
                end
            else
                if firstVis
                    b2 = b;
                    firstVis = false;
                end
            end
        end
    end

    function name = uniqueEncodeFeatures(config)
        cRanking = arrayfun(@char, config.rankingFeatures, 'UniformOutput', false);
        cClustering = arrayfun(@char, config.clusterFeatures, 'UniformOutput', false);
        
        name = [cRanking{:} ...
                '&' cClustering{:} ...
                '\_' rankingMetricToCode(config.rankingMetric) ...
                '\_' clusteringAlgorithmToCode(config.clustering)]; 
    end

    function [bestP, bestCR, bestF1] = plotResultSet(results, cutoffPoint, typeSuffix)
        bestP = plotResultSetSingleMetric(results, 'precision', 'rankingFeatures', ...
            'Precision', ['p' num2str(cutoffPoint) typeSuffix], cutoffPoint);
        
        bestCR = plotResultSetSingleMetric(results, 'clusterRecall', 'clusterFeatures', ...
            'Cluster Recall', ['cr' num2str(cutoffPoint) typeSuffix], cutoffPoint);
        
        bestF1 = plotResultSetSingleMetric(results, 'f1', 'clusterFeatures', ...
            'F1 Measure', ['f1_' num2str(cutoffPoint) typeSuffix], cutoffPoint);
    end

    
    function best = plotResultSetSingleMetric(results, metricName, featuresName, metricTitle, filename, cutoffPoint)
        extract = @(arr, name) arrayfun(@(x) getfield(x, name), arr);
        extract_cell = @(arr, name) arrayfun(@(x) getfield(x, name), arr, 'UniformOutput', false);
        
        resultMap = containers.Map;
        
        % Filter results by their cutoffpoints and maximum values
        for m=1:numel(results)
           current_results = results{m};

           extracted_result = current_results(arrayfun(@(x) x.cutoffPoint == cutoffPoint, current_results));

           mapKey = uniqueEncodeFeatures(extracted_result.configuration);
           
           currentVal = 0;
           if resultMap.isKey(mapKey)
               currentVal = getfield(resultMap(mapKey), metricName);
           end

           if getfield(extracted_result, metricName) > currentVal
              resultMap(mapKey) = extracted_result;
           end
        end
        
        results = resultMap.values();
        results = [results{:}];
        
        figure('Color', [1 1 1]);
        hold on;

        %titleString = [metricTitle ' @' num2str(cutoffPoint)];
        %title(titleString, 'fontWeight', 'bold');

        values = extract(results, metricName);

        % Sort by value, descending
        [~, valIdx] = sort(values, 2, 'descend');
        results = results(valIdx);
        
        maxResults = 10;
        % Limit to reasonable plot size, only plot the best candidates
        if numel(results) > maxResults
           results = results(1:maxResults); 
        end
        
        best = results(1);
        
        cfgs = [results.configuration];
        featureTypes = extract_cell(cfgs, featuresName);
        featureTypes = [featureTypes{:}];
        
        createBarPlotWithClasses(extract(results, metricName), featureTypes, ...
            {colorPalette{1} bluePalette{1:2}});
        
        ylim([0 1]);
        ylabel(metricTitle);
        
        n = arrayfun(@uniqueEncodeFeatures, cfgs, 'UniformOutput', false);

        set(gca, 'XTick', 1:numel(results), 'XTickLabel', n);
        Lib.rotateXLabels(gca(), -45);

        hold off;

        exportFigureAsPDF(filename);
    end
end

