classdef EvaluationResult
    
    properties
        cutoffPoint;
        precision;
        clusterRecall;
        f1;
        
        % The configuration used to obtain the given results.
        configuration;
    end
end

