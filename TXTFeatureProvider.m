classdef TXTFeatureProvider
    %TFIDFFEATUREPROVIDER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant = true, Access = private)
        TFIDFPath = '*_textTermsPerPOI.txt';
    end
    
    properties (SetAccess = private)
        type;
    end
    
    properties (Access = private)
        scoreProvider;
        fieldNames;
    end
    
    methods (Access = private, Static)
        function path = pathQueryForType(type)
            switch(type)
            case FeatureType.TFIDF
                path = TXTFeatureProvider.TFIDFPath;
            otherwise
                ex = MException('TXTFeatureProvider:FeatureUnsupported', ...
                                ['The feature ', char(type), ...
                                ' is not supported.']);
                throw(ex);
            end
        end
        
        function filepath = filepathForType(datasetPath, type)
            typeQuery = TXTFeatureProvider.pathQueryForType(type);
            
            files = dir(fullfile(datasetPath, ...
                     'desctxt', ...
                     typeQuery));
            
            if numel(files) == 0
                ex = MException('TXTFeatureProvider:FeatureFileNotFound', ...
                                ['The file for featuretype ' char(type) ...
                                ' was not found. [ pathQuery = ' ...
                                fullfile(datasetPath, ...
                                'desctxt', ...
                                typeQuery)]);
                throw(ex);
            end
                 
            file = files(1);         
            filepath = fullfile(datasetPath, ...
                        'desctxt', ...
                        file.name);
                    
            if exist(filepath, 'file') ~= 2 
                ex = MException('TXTFeatureProvider:FeatureFileNotFound', ...
                                ['The file for featuretype ' char(type) ...
                                ' was not found. [ path = ' filepath]);
                throw(ex);
            end
        end
    end
    
    methods
        function self = TXTFeatureProvider(type, datasetPath, fieldNames)
            self.type = type;
            self.fieldNames = fieldNames;
            self.scoreProvider = TXTScoreProvider(datasetPath, ...
                TXTFeatureProvider.filepathForType(datasetPath, type));
        end
        
        function feature = getFeature(self, query, image)
            if image.isWikiImage
                feature = self.scoreProvider.getReferenceVector(query);
            else
                if iscell(self.fieldNames)
                    for i=1:numel(self.fieldNames)
                        if ~exist('doc','var')
                            doc = [getfield(image, self.fieldNames{i})];
                        else
                            doc = [doc ' ' getfield(image, self.fieldNames{i})];
                        end
                    end
                else
                    doc = getfield(image, self.fieldNames);
                end

                feature = self.scoreProvider.getFeature(query, ...
                    regexp(doc, ' ', 'split'));
            end
        end
    end
    
end

