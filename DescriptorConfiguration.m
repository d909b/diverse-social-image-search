classdef DescriptorConfiguration
    
    properties
        clusterFeatures = [];
        rankingFeatures = [];
        
        % Defines the reranking algorithm to be used. One of:
        % Distance
        reranking;
        
        % Defines the ranking metric to be used. Any of the distance
        % metrics supported by __pdist__, 'histogram-intersection',
        % 'cosine-similarity' and 'chi2'
        rankingMetric;
        
        % Defines the cluster algorithm to be used. One of:
        % KMeans, AHC and MeanShift.
        clustering;
        
        % Defines the metric to be used for clustering. May be one of
        % 'cityblock', 'euclidean' and 'chi2'.
        clusteringMetric;
        
        % Whether or not you wish to filter out images for which the opencv
        % face detector found faces.
        filterFaces;
        
        % Defines the feature vector postprocessing method after the
        % feature concatenation. May be one of 'None', 'Normalize' and
        % 'Decorrelate'.
        rankingPostProcessing;
        clusteringPostProcessing;
    end
    
    methods (Static)
        function self = defaultDescriptorConfiguration()
            self.rankingFeatures = [FeatureType.TFIDF];
            self.reranking = 'Distance';
            self.rankingMetric = 'cosine-similarity';
            self.rankingPostProcessing = 'None';
            
            self.clusterFeatures = [FeatureType.SIFT, FeatureType.LBP3x3];
            self.clustering = 'AHC';
            self.clusteringMetric = 'euclidean';
            self.clusteringPostProcessing = 'None';
            
            self.filterFaces = true;
        end
    end
end

