classdef ImageItem
    properties (SetAccess = private)
        imagePath;
        imageID;
        userName;
        title;
        views;
        tags;
        rank;
        numComments;
        longitude;
        latitude;
        description;
        creationDate;
        isWikiImage;
        numPeople;
    end
    
    properties
        relevance; 
    end
    
    methods (Access = private, Static)
        function value = getPhotosCached(xmlFile)
            persistent domCache;
            
            if isempty(domCache)
               domCache = java.util.HashMap; 
            end
            
            if ~domCache.containsKey(xmlFile)
                doc = xmlread(xmlFile);
                
                photos = doc.getElementsByTagName('photo');
            
                dataMap = java.util.HashMap;
                for i=0:photos.getLength-1
                    photo = photos.item(i);
                    dataMap.put(char(photo.getAttribute('id')), photo);
                end
                
                domCache.put(xmlFile, dataMap);
            end
            
            value = domCache.get(xmlFile);
        end
        
        function imageItem = parseXMLMetaData(imageItem, xmlFile)
            photos = ImageItem.getPhotosCached(xmlFile);
            
            photo = photos.get(imageItem.imageID);
            
            imageItem.userName = char(photo.getAttribute('username'));
            imageItem.title = char(photo.getAttribute('title'));
            imageItem.tags = native2unicode(uint8(photo.getAttribute('tags').getBytes()'));
            imageItem.views = str2double(char(photo.getAttribute('views')));
            imageItem.rank = str2double(char(photo.getAttribute('rank')));
            imageItem.numComments = str2double(char(photo.getAttribute('nbComments')));
            imageItem.longitude = str2double(char(photo.getAttribute('longitude')));
            imageItem.latitude = str2double(char(photo.getAttribute('latitude')));
            imageItem.description = native2unicode(uint8(photo.getAttribute('description').getBytes()'));
            imageItem.creationDate = ...
                datenum(char(photo.getAttribute('date_taken')), ...
                        'yyyy-mm-dd HH:MM:SS');

            imageItem.isWikiImage = false;
        end
    end
    
    methods
        function self = ImageItem(datasetPath, query, id, isWikiImage)
            if nargin < 4
               isWikiImage = false; 
            end
            
            self.imageID = id;
            self.relevance = 0.0;
            self.isWikiImage = isWikiImage;
            
            if isWikiImage
                imgFolder = fullfile('imgwiki', query); 
            else
                imgFolder = fullfile('img', query);
            end
            
            self.imagePath = fullfile(datasetPath, imgFolder, [id '.jpg']);
            
            fd = fopen(self.imagePath, 'r');
            
            if fd == -1
                warning(['Filepath ' self.imagePath ' cant be opened. name = ' id]);
            else
                c = onCleanup(@()fclose(fd));
            end
            
            if isWikiImage && ~isempty(strfind(id, ','))
                warning(['Invalid image at ' self.imagePath ...
                    ' -- Contains a comma value, which will lead to errors with the provided CSV files. imageID = ' id]);
            end
            
            self.numPeople = Utility.detect_people(self.imagePath);
            
            if ~isWikiImage
                xmlPath = fullfile(datasetPath, 'xml', [query '.xml']);
                self = ImageItem.parseXMLMetaData(self, xmlPath);
            end
        end

    end
end

