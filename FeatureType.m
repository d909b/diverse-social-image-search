classdef FeatureType
    % FEATURETYPE Defines the supported feature types.
    %
    % Author: Patrick Schwab
    
    enumeration
        Baseline, ...
        % Textual Features - applied on title, tags and description:
        TFIDF, ...
        % Visual Features (Global):
        CN, HOG, CM, LBP, CSD, GLRLM, ...
        % Visual Features (Local):
        CN3x3, CM3x3, LBP3x3, GLRLM3x3, ...
        % Custom Features:
        SIFT
    end
    
    methods (Static)
        function retVal = isTextual(type)
            retVal = type == FeatureType.TFIDF || ...
                     type == FeatureType.Baseline;
        end
        
        function retVal = isVisual(type)
            retVal = ~FeatureType.isTextual(type);
        end
    end
end

