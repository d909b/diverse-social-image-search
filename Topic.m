classdef Topic
    
    properties (SetAccess = private)
        name;
        number;
        longitude;
        latitude;
        wiki;
        images;
        wikiImages;
    end
    
    methods (Access = private, Static)                   
        function topic = parseXMLMetaData(topic, xmlFile)
            doc = xmlread(xmlFile);
            
            topics = doc.getElementsByTagName('topic');
            
            for i=0:topics.getLength-1
                t = topics.item(i);
                
                title = Utility.XMLHelper.dataForTagName(t, 'title');
                
                if strcmp(title, topic.name) == 1
                    topic.number = char(Utility.XMLHelper.dataForTagName(t, ...
                        'number'));
                    topic.longitude = str2double(char(Utility.XMLHelper.dataForTagName(t, ...
                        'longitude')));
                    topic.latitude = str2double(char(Utility.XMLHelper.dataForTagName(t, ...
                        'latitude')));
                    topic.wiki = char(Utility.XMLHelper.dataForTagName(t, 'wiki'));
                    break;
                end
            end
        end
        
        function images = loadImagesForTopic(topic, dataset)
            xmlPath = fullfile(dataset, 'xml', [topic.name '.xml']);
            doc = xmlread(xmlPath);
            
            photos = doc.getElementsByTagName('photo');
            
            for i=0:photos.getLength-1
                disp(['Loading topic ' topic.name ' - image #' num2str(i+1) ...
                      ' / ' num2str(photos.getLength)]);
                
                photo = photos.item(i);
                
                id = char(photo.getAttribute('id'));
                images(i+1) = ImageItem(dataset, topic.name, id);
            end
        end
    end
    
    methods
        function self = Topic(datasetPath, name)
            self.name = name;
            
            % Find the topics file for this data set
            files = dir(fullfile(datasetPath, ...
                        '*_topics.xml'));
            xmlPath = fullfile(datasetPath, files(1).name);
            
            self = Topic.parseXMLMetaData(self, xmlPath);
            self.images = Topic.loadImagesForTopic(self, datasetPath);
            
            wikiPath = fullfile(datasetPath, 'imgwiki', name);
            files = dir(wikiPath);
            
            self.wikiImages = ImageItem.empty(0,0);
            for i=3:numel(files) % First two files are . and ..
                imgname = files(i).name;
                [~, imgname, ~] = fileparts(imgname);
                
                self.wikiImages(end+1) = ImageItem(datasetPath, name, imgname, true);
            end
        end
        
        function [] = setImages(self, imgs)
            self.images = imgs;
        end
    end
    
end

