classdef viola
    %CLASS VIOLA Face detection through Viola-Johnes detection
    %   calls opencv through mex function, and provides convinience
    %   functions
    
    properties (SetAccess = private, GetAccess = public)
        filename = '';   % image file
        cascadefile = '';
        faces = [];      % data from opencv
                         % [x1,y1,x2,y2,width, height, scale; number of Faces]
    end
    
    properties (SetAccess = private, GetAccess = public, Dependent = true)
        nFaces;    %dependent var, gives number of faces
    end
    
    methods
        function this = viola(fname, cascadef)  %constructor
            if (nargin==1)
                this.filename = fname;
                this.faces = detect(fname);
                return;
            end
            if (nargin==2)
                this.filename = fname;
                this.cascadefile = cascadef;
                this.faces = detect(fname, cascadef);
                return;
            end
            if (nargin>2)
                error('viola(): wrong number of arguments');
            end
        end
        
        function n = get.nFaces(this) %dependent var function
            n = size(this.faces,2);
        end
        
        function this = detect(this, fname)
            if (nargin>1)
                this.filename = fname;
            end         
            
            this.faces = detect(this.filename);
        end
        
        function drawRec(this, I)
            if (nargin>1)
                imshow(I);
            else
                I = imread(this.filename);
                imshow(I);
            end
            
            imHeight = size(I,1);
            imWidth = size(I,2);
            
            hold on;
            if (this.nFaces<1)
                return
            end
            
            for i=1:this.nFaces
                
                h = rectangle('Position', [(this.faces(1,i)) (this.faces(2,i)) this.faces(5,i) this.faces(6,i)], ...
                              'LineWidth', 5, ...
                              'EdgeColor', 'w');
                h = rectangle('Position', [(this.faces(1,i)) (this.faces(2,i)) this.faces(5,i) this.faces(6,i)], ...
                  'LineWidth', 2, ...
                  'EdgeColor', 'b');
            end
            
            hold off;
        end
        
        function drawRecForSkin(this, I)
            %if (nargin>1)
            %    imshow(I);
            %else
            %    I = imread(this.filename);
            %    imshow(I);
            %end
            
            %imHeight = size(I,1);
            %imWidth = size(I,2);
            
            hold on;
            if (this.nFaces<1)
                return
            end
            
            for i=1:this.nFaces
                
                width = this.faces(5,i);
                wstep = round(width/6);

                height = this.faces(6,i);
                hstep = round(height/3);

                x1 = this.faces(1,i)+ hstep;
                y1 = this.faces(2,i)+ wstep;
                %x2 = this.faces(3,i) - wstep;
                %y2 = this.faces(4,i) - hstep;
                h1 = width-(2*wstep);
                w1 = height-(2*hstep);
                

                %axis equal
                %daspect([1,1,1])
                
                h = rectangle('Position', [x1, y1, w1, h1], ...
                              'LineWidth', 5, ...
                              'EdgeColor', 'w');
                h = rectangle('Position', [x1, y1, w1, h1], ...
                  'LineWidth', 2, ...
                  'EdgeColor', 'r');
            end
            
            hold off;
        end
        
    end
    
end

