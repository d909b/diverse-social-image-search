if you have some libtiff version problems on Mac, do the following:

cd /Applications/MATLAB74/bin/maci/   (or wherever your Matlab is installed)
mv libtiff.3.7.1.dylib  libtiff.3.7.1.dylib.MATLAB  (give it a different name)
ln -s /opt/local/lib/libtiff.3.dylib libtiff.3.7.1.dylib  (create a soft link to the Imagemagick library file)