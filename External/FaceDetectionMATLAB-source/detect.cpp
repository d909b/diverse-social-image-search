
#include "mex.h"

// requires the OpenCV library to compile 

#ifndef _EiC
#include <cv.h>
#include <highgui.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>
#endif

#ifdef _EiC
#define WIN32
#endif

static CvMemStorage* storage = 0;
static CvHaarClassifierCascade* cascade = 0;

void detect_and_draw( IplImage* image );

const char* cascade_name ="mex/detect/haarcascade_frontalface_alt.xml";

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    CvCapture* capture = 0;
    IplImage *myImage;
    double **data2, *outArray;
    int scale = 1;
    int i,j;
    //Declarations
    mxArray *thisfilenameData;
    int thisfilenameLength;
    char *thisfilename;
    
    mxArray *cascade_filenameData;
    char *cascade_filename;
    int cascade_filenameLength;
    
    thisfilenameData = (mxArray *)(prhs[0]);
    
    if (nrhs>1) {
        cascade_filenameData = (mxArray *)(prhs[1]);
        //mexErrMsgTxt("nrhs<>1\n");
        cascade_filenameLength = (int)(mxGetN(cascade_filenameData)+1);
        cascade_filename = (char*)(mxCalloc(cascade_filenameLength, sizeof(char))); //mxCalloc is similar to malloc in C
        mxGetString(cascade_filenameData,cascade_filename,cascade_filenameLength);
        
        //mexPrintf("using ");
        //mexPrintf(cascade_filename);
        //mexPrintf("\n");
    }
    

   
    
    thisfilenameLength = (int)(mxGetN(thisfilenameData)+1);
    thisfilename = (char*)(mxCalloc(thisfilenameLength, sizeof(char))); //mxCalloc is similar to malloc in C
    mxGetString(thisfilenameData,thisfilename,thisfilenameLength);
   
    if (nrhs==1) {
        cascade = (CvHaarClassifierCascade*)cvLoad( cascade_name, 0, 0, 0 ); 
    } else {
        cascade = (CvHaarClassifierCascade*)cvLoad( cascade_filename, 0, 0, 0 ); 
    }
            
    if( !cascade )
    {
        //mexErrMsgTxt(cascade_name);
		mexErrMsgTxt("ERROR: Could not load classifier cascade\n");
    }
    storage = cvCreateMemStorage(0);
    myImage = cvLoadImage( thisfilename, 1 );

    if( cascade )
    {
        CvSeq* faces = cvHaarDetectObjects( myImage, cascade, storage,1.1, 2, CV_HAAR_DO_CANNY_PRUNING,cvSize(40, 40) );

        data2 = (double**) mxMalloc ((faces->total)* sizeof(double));        
        for (i=0;i<faces->total;i++)
            data2[i] = (double*) mxMalloc (7*sizeof(double));

        for( i = 0; i < (faces ? faces->total : 0); i++ )
        {
            CvRect* r = (CvRect*)cvGetSeqElem( faces, i );
            data2[i][0] = r->x*scale; //x1
            data2[i][1] = r->y*scale; //y1
            
            data2[i][2] = (r->x+r->width)*scale; //x2
            data2[i][3] = (r->y+r->height)*scale; //y2
            
            data2[i][4] = (r->width)*scale; //widht
            data2[i][5] = (r->height)*scale; //height
            data2[i][6] = scale; //scale
        }
        
        plhs[0] = mxCreateDoubleMatrix(7, faces->total, mxREAL);
        outArray = mxGetPr(plhs[0]);
        
        for(i=0;i<faces->total;i++)
        {
            for(j=0;j<7;j++)
            {
                outArray[(i*7)+j] = data2[i][j];
            }
        }

        //mxSetPr(plhs[0],data2);
    }
     
    cvClearMemStorage( storage );
	cvReleaseHaarClassifierCascade( &cascade );
    cvReleaseImage( &myImage );
}
