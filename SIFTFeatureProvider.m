classdef SIFTFeatureProvider
    %SIFTFEATUREPROVIDER Provides SIFT feature vectors (Bag of Visual Words) for images.
    %
    % Author: Patrick Schwab
    
    properties (Constant)
       vocabularySize = 512; 
    end
    
    properties (Access = private)
        datasetPath;
        cache;
    end
    
    properties (SetAccess = private)
        type;
    end
    
    methods(Static, Access = private)
        function [] = setupVLFeat()
            % Setup vlfeat if it hasnt already been setup.
            try
                vlversion verbose;
            catch err
                run('External/vlfeat-0.9.18/toolbox/vl_setup');
            end
            
            vl_threads(4);
        end
        
        function cache = parseSIFTDescriptors(topics)
            SIFTFeatureProvider.setupVLFeat();
            
            cache = java.util.HashMap;
            for i=1:numel(topics)
                topic = topics(i);
                
                localCache = java.util.HashMap;
                
                imgs = [topic.images, topic.wikiImages];
                desc = cell(size(imgs));
                
                % Extract all the SIFT descriptors
                for j=1:numel(imgs)
                    img = imgs(j);
                    
                    I = imread(img.imagePath);
                    
                    if size(I, 3) == 3
                        I = rgb2gray(I);
                    end
                    
                    [~, desc{j}] = vl_sift(single(I), 'PeakThresh', 7.5);
                end
                
                % Find the cluster centroids to use as references for our
                % visual bag of words model
                centroids = vl_kmeans(single(cell2mat(desc)), SIFTFeatureProvider.vocabularySize, ...
                    'algorithm', 'ann', 'Initialization', 'plusplus', ...
                    'MaxNumComparisons', ceil(SIFTFeatureProvider.vocabularySize / 48));
                
                % Build a kdtree of these centroids for more efficient
                % nearest neighbour queries.
                tree = KDTreeSearcher(centroids');
                
                for j=1:numel(imgs)
                    descriptors = single(desc{j}');
                    
                    idx = knnsearch(tree, descriptors);
                    localCache.put(imgs(j).imageID, uint16(idx));
                end
                
                disp(['Computed BoVW model for ' topic.name '.']);
                
                cache.put(topic.name, localCache);
            end
        end
        
        function cache = getCachedDescriptors(datasetPath, topics)
            cachePath = fullfile(CombinedImageRanker.cacheDirectory, ...
                     [urlencode(datasetPath) '_sift.mat']);
            try
                load(cachePath, 'cache');
            catch err
                cache = SIFTFeatureProvider.parseSIFTDescriptors(topics);
                save(cachePath, 'cache');
            end
        end
    end
    
    methods
        function self = SIFTFeatureProvider(datasetPath, topics)
            self.type = FeatureType.SIFT;
            self.cache = SIFTFeatureProvider.getCachedDescriptors(datasetPath, topics);
        end
        
        function feature = getFeature(self, query, image)
            idx = self.cache.get(query).get(image.imageID);
            uidx = unique(idx);
            counts = histc(idx, uidx);
            feature = zeros(1, SIFTFeatureProvider.vocabularySize);
            feature(uidx) = counts ./ numel(idx);
        end
    end
    
end

