classdef VISFeatureProvider < handle
    %VISFEATUREPROVIDER Provides feature vectors for the visual features
    %   given in a MediaEval dataset.
    %
    % Author: Patrick Schwab
    
    properties (Constant = true, Access = private)
        % Global
        CNPath = '*CN.csv';
        HOGPath = '*HOG.csv';
        CMPath = '*CM.csv';
        LBPPath = '*LBP.csv';
        CSDPath = '*CSD.csv';
        GLRLMPath = '*GLRLM.csv';
        
        % Local
        CN3x3Path = '*CN3x3.csv';
        CM3x3Path = '*CM3x3.csv';
        LBP3x3Path = '*LBP3x3.csv';
        GLRLM3x3Path = '*GLRLM3x3.csv';
    end
    
    properties (Access = private)
        fieldName;
        datasetPath;
        cache;
        referenceCache;
    end
    
    properties (SetAccess = private)
        type;
    end
    
    methods(Static, Access = private)
        function cache = parseReferenceDescriptors(type, datasetPath)
            typeQuery = VISFeatureProvider.pathQueryForType(type);
            
            folderpath = fullfile(datasetPath, 'descvis', 'imgwiki');
            files = dir(fullfile(folderpath, typeQuery));
                 
            if numel(files) == 0
               ex = MException('VISFeatureProvider:FeatureFileNotFound', ...
                                ['The file for featuretype ' char(type) ...
                                ' was not found. [ path = ' filepath ' ]']);
               throw(ex);
            end
            
            cache = containers.Map;
            for i=1:numel(files)
                name = files(i).name;
                cache(name(1:end-numel(typeQuery))) = ...
                   VISFeatureProvider.parseVISDescriptor(fullfile(folderpath, ...
                                                                  name));
            end
        end
        
        function cache = parseVISDescriptors(type, datasetPath)
            typeQuery = VISFeatureProvider.pathQueryForType(type);
            
            folderpath = fullfile(datasetPath, 'descvis', 'img');
            files = dir(fullfile(folderpath, typeQuery));
                 
            if numel(files) == 0
               ex = MException('VISFeatureProvider:FeatureFileNotFound', ...
                                ['The file for featuretype ' char(type) ...
                                ' was not found. [ path = ' filepath ' ]']);
               throw(ex);
            end
            
            cache = containers.Map;
            for i=1:numel(files)
                name = files(i).name;
                cache(name(1:end-numel(typeQuery))) = ...
                   VISFeatureProvider.parseVISDescriptor(fullfile(folderpath, ...
                                                                  name));
            end
        end
        
        function cache = parseVISDescriptor(filepath)
        %PARSEVISDESCRIPTOR Parses a textual descriptor file (*CM.csv, 
        % *CM3x3.csv,..) of the devset into a map of (key-value)
        %entries. Mapping the imageID to its feature vector.
            try
                % Ignore the first column with csvread, because it may
                % contain a string.
                data = csvread(filepath, 0, 1);
            catch err
                ex = MException('VISFeatureProvider:FeatureFileNotReadable', ...
                                ['The descriptor file could not be opened.' ...
                                '[ path = ' filepath ']']);
                throw(ex);
            end
            
            fd = fopen(filepath, 'r');
            
            if fd == -1
                ex = MException('VISFeatureProvider:FeatureFileNotReadable', ...
                                ['The feature file for ', char(featureType), ...
                                ' could not be opened. [ path = ', filepath, ...
                                ' ]']);
                throw(ex); 
            end
            
            c = onCleanup(@()fclose(fd));
            
            frewind(fd);
            
            firstCol = textscan(fd,'%s %*[^\n]', 'delimiter', ',');
            
            numRows = size(data,1);
            keys = firstCol;
            values = cell(1,numRows);
            
            for i=1:numRows
                % The first column is the image ID, all subsequent columns
                % form the feature vector.
                values{i} = data(i, :);
            end
            
            cache = containers.Map(keys{1}', values);
        end
        
        function cache = getCachedReferenceDescriptors(type, datasetPath)
            cachePath = fullfile(CombinedImageRanker.cacheDirectory, ...
                     [urlencode(datasetPath) '_' char(type) '_reference.mat']);
            try
                load(cachePath, 'cache');
            catch err
                cache = VISFeatureProvider.parseReferenceDescriptors(type, datasetPath);
                save(cachePath, 'cache');
            end
        end
        
        function cache = getCachedDescriptors(type, datasetPath)
            cachePath = fullfile(CombinedImageRanker.cacheDirectory, ...
                     [urlencode(datasetPath) '_' char(type) '.mat']);
            try
                load(cachePath, 'cache');
            catch err
                cache = VISFeatureProvider.parseVISDescriptors(type, datasetPath);
                save(cachePath, 'cache');
            end
        end
        
        function path = pathQueryForType(type)
            switch(type)
            case FeatureType.CN
                path = VISFeatureProvider.CNPath;
            case FeatureType.HOG
                path = VISFeatureProvider.HOGPath;
            case FeatureType.CM
                path = VISFeatureProvider.CMPath;
            case FeatureType.LBP
                path = VISFeatureProvider.LBPPath;
            case FeatureType.CSD
                path = VISFeatureProvider.CSDPath;
            case FeatureType.GLRLM
                path = VISFeatureProvider.GLRLMPath;
            case FeatureType.CN3x3
                path = VISFeatureProvider.CN3x3Path;
            case FeatureType.CM3x3
                path = VISFeatureProvider.CM3x3Path;
            case FeatureType.LBP3x3
                path = VISFeatureProvider.LBP3x3Path;
            case FeatureType.GLRLM3x3
                path = VISFeatureProvider.GLRLM3x3Path;
            otherwise
                ex = MException('VISFeatureProvider:FeatureUnsupported', ...
                                ['The feature ', char(featureType), ...
                                ' is not supported.']);
                throw(ex);
            end
        end
    end
    
    methods
        function self = VISFeatureProvider(type, datasetPath)
            self.type = type;
            self.datasetPath = datasetPath;
            self.cache = VISFeatureProvider.getCachedDescriptors(type, ...
                                                                 datasetPath);
                                                            
            self.referenceCache = ...
                VISFeatureProvider.getCachedReferenceDescriptors(type, ...
                                                                 datasetPath);
        end
        
        function feature = getFeature(self, query, image)
            if ~image.isWikiImage
                features = self.cache(query);
            else
                features = self.referenceCache(query);
            end
            
            feature = features(image.imageID);
        end
    end
end

