classdef KMeans
    %KMEANS Clusters images using the k-means [1] algorithm.
    %
    % [1] J. B. MacQueen: Some Methods for classification and Analysis of
    %   Multivariate Observations. 1967
    %
    % Author: Patrick Schwab
    
    properties(Constant, Access = private)
        % Supposedly the best performing __k__ for cutoff point X=10,
        % according to [2] "MUCKE Participation at Retrieving Diverse 
        % Social Images Task of MediaEval 2013" by Armagan, Popescu and
        % Duygulu, 2013, UPDATED 2014: Was 15 in 2013
        numClusters = 25;
    end
    
    methods
        function self = KMeans()
            % Setup vlfeat if it hasnt already been setup.
            try
                vlversion verbose;
            catch err
                run('External/vlfeat-0.9.18/toolbox/vl_setup');
            end
            
            vl_threads(4);
        end
        
        function clusters = cluster(self, images, features, metric)
            if strcmpi(metric, 'euclidean')
                metric = 'L2';
            elseif strcmpi(metric, 'cityblock')
                metric = 'L1';
            end
            
            [~, data2cluster] = vl_kmeans(single(features)', Clusterer.KMeans.numClusters, ...
                    'algorithm', 'lloyd', 'Initialization', 'plusplus', 'Distance', metric);
            
            clusters = Utility.clusters_by_indices(images, data2cluster);
        end
    end
    
end

