classdef MeanShift
    %MEANSHIFT Clusters images using the meanshift [1] algorithm.
    %
    % [1] Fukunaga, K.; Hostetler, L.,
    %   "The estimation of the gradient of a density function, 
    %   with applications in pattern recognition," Information Theory, 1975
    %
    % Author: Patrick Schwab
    
    properties
    end
    
    methods
        function self = MeanShift()
        end
        
        function clusters = cluster(self, images, features, metric)
            % MeanShiftCluster uses a randomly initialized starting node.
            % Therefore, we fix the random seed to make results reproducable.
            rng(99);
            
            % TODO: Employ a bandwidth selection algorithm
            bandwidth = 0.125;
            [~, data2cluster] = Lib.MeanShiftCluster(features', bandwidth);
            
            clusters = Utility.clusters_by_indices(images, data2cluster);
        end
    end
    
end

