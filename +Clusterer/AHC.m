classdef AHC
    %AHC Clusters images using an Agglomerative Hierarchical Clustering 
    %   (AHC) algorithm.
    %
    % Author: Patrick Schwab
    
    properties(Constant, Access = private)
        numClusters = 25;
    end
    
    methods
        function self = AHC()
        end
        
        function clusters = cluster(self, images, features, metric)
            if strcmpi(metric, 'L1')
                metric = 'euclidean';
            elseif strcmpi(metric, 'chi2')
                metric = {@(X,Y) vl_alldist2(X', Y', 'CHI2')};
            elseif strcmpi(metric, 'cosine-similarity')
                metric = 'cosine';
            end
            
            z = linkage(features, 'single', metric);
            data2cluster = cluster(z, 'maxclust', Clusterer.AHC.numClusters);
            
            clusters = Utility.clusters_by_indices(images, data2cluster);
        end
    end
    
end

