classdef IntersectionClusterRanker
    %INTERSECTIONCLUSTERRANKER 
    %
    % Author: Patrick Schwab
    
    properties (Access = private)
       cache; 
    end
    
    methods (Access = private)
        function count = countNumberOfContainedImages(self, cluster, preselected)
            count = 0;
            for image=preselected
                for other=cluster.images
                   if strcmp(other.imageID, image.imageID)
                       count = count + 1;
                       break;
                   end
                end
            end
        end
        
        function cache = buildCache(self, clusterSet)
            cache = java.util.HashMap;
            
            images = [clusterSet{1}.images];
            
            for image=images
                clusterIndices = ones(1, numel(clusterSet));
                
                for i=1:numel(clusterSet)
                     clusters = clusterSet{i};
                     
                     for idx=1:numel(clusters)
                         if self.countNumberOfContainedImages(clusters(idx), image)
                             clusterIndices(i) = idx;
                             break; 
                         end
                     end
                end
                
                cache.put(image.imageID, clusterIndices);
            end
        end

        function diversity = rateDiversitySingle(self, clusterSet, image, preselected)
            diversity = 0;
            if ~isempty(preselected)
                indices = self.cache.get(image.imageID);

                for other=preselected
                   diversity = diversity + sum(indices == self.cache.get(other.imageID));
                end

                diversity = diversity / numel(preselected) / numel(clusterSet);
            end
        end

        function diversity = rateDiversity(self, clusterSet, images, preselected)
            diversity = arrayfun(@(img) self.rateDiversitySingle(clusterSet, img, preselected), images);
        end
    end

    methods
        function self = IntersectionClusterRanker()
        end

        function images = rank(self, clusterSet)
            self.cache = self.buildCache(clusterSet);

            imgStack = [clusterSet{1}.images];
            images = ImageItem.empty(0,0);
            while numel(images) < 50
                diversities = self.rateDiversity(clusterSet, imgStack, images);
                relevances = [imgStack.relevance];

                combined = diversities + relevances; % same weights
                idx = find(combined == min(combined));

                images(end+1) = imgStack(idx(1));
                imgStack(idx(1)) = [];
            end
            [~, idx] = sort([imgStack.relevance]);
            images = horzcat(images, imgStack(idx));
        end
    end
end

